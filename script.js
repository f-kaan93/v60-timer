var beansWeight = 12;
var waterWeight = 180;
var steps = [36, 72, 108, 144, 180];
var activeStep = -1;
var timeElapsed = 0;
var pouring = false;
var intervalHandler = undefined;

function setSteps() {
    steps.forEach(function (step, index) {
        steps[index] = parseInt(waterWeight * ((index + 1) / 5));
    })
}
function render() {
    document.getElementById('beansWeight').value = beansWeight;
    document.getElementById('waterWeight').value = waterWeight;
    steps.forEach(function (step, index) {
        var stepChild = document.getElementsByClassName('steps')[0].children[index];
        stepChild.innerHTML = step;
        if (activeStep == index) {
            stepChild.classList.add('active');
        } else {
            stepChild.classList.remove('active');
        }
    });
    if (pouring) {
        document.getElementsByClassName('adjust-container')[0].classList.remove('active');
        document.getElementById('timer').innerHTML = timeElapsed < 180 ? 45 - (timeElapsed % 45) : 'Last pour';

    } else {
        document.getElementsByClassName('adjust-container')[0].classList.add('active');
        document.getElementById('timer').innerHTML = 'Start';
    }
}
var animationSettings = {
    duration: 1000, 
    easing: 'easingExponentialOut'
}
function wavesForwards() {
    KUTE.to('.ed070f02-a6d6-430a-a198-5d9756408f2d', { path: 'M527.73,524.43c25-33.85,82.21-101,179.17-136.84,186.49-68.82,354.39,39.67,374.1,52.86v458.3c-63.37,39.09-143.34,62.42-230.27,62.42-138.15,0-258.73-58.91-323-146.4V524.43' }, animationSettings).start();
    KUTE.to('.f642602d-0482-41dd-aa1c-a899bd004838', { path: 'M.5,396.87c60.82-21.2,157.76-47.45,276.45-44.48,216.37,5.4,309.38,103,505.27,97.36,69.69-2,173.17-17.68,298.78-87.38v685H0l.5-650.5' }, animationSettings).start();
}
function wavesBackwards() {
    KUTE.to('.ed070f02-a6d6-430a-a198-5d9756408f2d', { path: 'M0,513.91c64.27-87.49,184.85-146.4,323-146.4,86.93,0,166.9,23.33,230.27,62.42v458.3C489.9,927.32,409.93,950.65,323,950.65c-138.15,0-258.73-58.91-323-146.4V513.91' }, animationSettings).start();
    KUTE.to('.f642602d-0482-41dd-aa1c-a899bd004838', { path: 'M.5,396.87c124,57.88,223.23,67.12,286.5,66.5,200.55-2,290.3-105.33,509-126,123.68-11.69,225.48,9,285,25v685H0l.5-650.5' }, animationSettings).start();
}
document.getElementById('beansWeight').onchange = function (event) {
    beansWeight = event.target.value;
    waterWeight = beansWeight * 15;
    setSteps();
    render();
}
document.getElementById('waterWeight').onchange = function (event) {
    waterWeight = event.target.value;
    beansWeight = parseInt(waterWeight / 15);
    setSteps();
    render();
}
document.getElementById('timer').onclick = function () {
    pouring = !pouring;
    if (!pouring) {
        clearInterval(intervalHandler);
        timeElapsed = 0;
        activeStep = -1;
        render();
        wavesBackwards();
    } else {
        timeElapsed = 0;
        activeStep = 0;
        render();
        intervalHandler = setInterval(function () {
            timeElapsed++;
            activeStep = Math.ceil(((timeElapsed + 1) / 45) - 1) > 4 ? 4 : Math.ceil(((timeElapsed + 1) / 45) - 1);
            render();
        }, 1000);
        wavesForwards();
    }
}


render();