var cacheName = 'v60-timer-0SnXIbZMjfP6';
var filesToCache = [
  '/',
  '/index.html',
  '/script.js',
  '/style.css',
  '/inobounce.js',
  'https://cdn.jsdelivr.net/npm/kute.js@2.0.2/dist/kute.min.js',
];
self.addEventListener('install', function (e) {
  e.waitUntil(caches.open(cacheName).then(function (cache) {
      return cache.addAll(filesToCache);
  }));
});

self.addEventListener('activate', function (event) {
  event.waitUntil(caches.keys().then(function (oldCaches) {
      return Promise.all(oldCaches.filter(function (oldCache) {
          if(oldCache!=cacheName)
          return true;
      }).map(function (oldCache) {
          return caches.delete(oldCache);
      }));
  }));
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
      caches.match(event.request).then(function(response) {
        return response || fetch(event.request);
      })
    );
 });
